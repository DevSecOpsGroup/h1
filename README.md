**H1**

Hackathon 1

*  Year-end 12-hr Ansible Hackathon (2018-12-31) organized by DevSecOps community.
*  '#H1' is the hashtag for groups messages

**Links**
*  email devsecops@groups.io
*  git   https://gitlab.com/DevSecOpsGroup/h1
*  wiki  https://gitlab.com/DevSecOpsGroup/h1/wikis/home
